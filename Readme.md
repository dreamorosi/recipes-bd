# Recipes Network
A network about recipes and their ingredients, heaven for picky eaters.
## Why?
We want to try to highlight correlations between recipes that are identified by a title (label) by looking at their categories/ingredients (nodes) and show how the two connect to each other. We are going to weight the connections or the nodes (depending on the visualization) by introducing a Jaccard index (more on this later).
## Data sources
Over 20k recipes listed by recipe rating, nutritional information and assigned category (sparse).
[[Source: Kaggle.com](https://www.kaggle.com/hugodarwood/epirecipes)]

## Process
The data is read from a json and loaded in a dataframe, then we create a copy of the dataset with including only the `title` and `categories` columns. After that we generate an uuid for each one to facilitate further processing.
We define a function that takes two lists of `categories` and returns the Jaccard index or distance. This index describes the similarity of two lists by dividing their intersection by the weighted union of the two. The resulting index expresses the similarity of the two lists where 0 is no element in common and 1 is total match.

## Next steps
* The datasets needs to be reduced.
* Create a matrix of similarity indexes by combining each recipe with each other.
* Save results in csv file.
* Define treshold of similarity to identify sample.
* Plot distribution of similarity.
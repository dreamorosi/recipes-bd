import pandas as pd
from uuid import uuid4
# from sklearn.metrics import jaccard_similarity_score
# from sklearn.metrics.pairwise import pairwise_distances
import numpy as np
from collections import Counter

filePath = '/Users/andre/Sites/recipes/recipes.json'


buff = ''
buff += open(filePath, 'rU').read()
json = pd.read_json(buff)
d = json[['title', 'categories']].copy()
d['uuid'] = d.index.to_series().map(lambda x: uuid4())


def jaccard_distance(list1, list2):
    a = Counter(list1)
    b = Counter(list2)
    union = list(set(a + b))
    intersection = list(set(a) - (set(a) - set(b)))
    # print "Union - %s" % union
    # print "Intersection - %s" % intersection
    return float(len(intersection)) / float(len(union))

# cumulative distribution of jaccard index to find threshold (log axis)


# dummies = np.asarray(d[24:25].categories)
# dummies2 = np.asarray(d[23:24].categories)
# print dummies[0]
# print dummies2[0]

# print jaccard_distance(dummies[0], dummies2[0])

def generate_list(d):
    d = d[0: 1000]

    f = open('/Users/andre/Sites/recipes/cvfile.csv', 'w')
    f.write('Source,Target,Similarity\n')

    for x in d.index.to_series():
        dummies = np.asarray(d[x:x + 1].categories)
        title1 = d[x:x + 1].iloc[0]['title'].encode('utf-8').strip().replace(',', '').replace('"', '')
        for y in d.index.to_series():
            if y > x:
                dummies2 = np.asarray(d[y:y + 1].categories)
                title2 = d[y:y + 1].iloc[0]['title'].encode('utf-8').strip().replace(',', '').replace('"', '')
                f.write(title1 + ',' + title2 + ',' + str(jaccard_distance(dummies[0], dummies2[0])) + '\n')



# d = pd.read_csv('/Users/andre/Sites/recipes/cvfile.csv')
# e = d[d['Similarity'] > 0.4]
# print d.shape
# print e.shape
#e.to_csv('/Users/andre/Sites/recipes/output.csv')
print d[['']]